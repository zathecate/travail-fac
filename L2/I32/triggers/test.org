* Ex 1
** 1
   #+BEGIN_SRC sql
  CREATE TABLE NbPlacesParType (_TypeEmpl varchar(30)_, NbEmpl integer);
   #+END_SRC
** 2
   #+BEGIN_SRC sql
  INSERT INTO NbPlacesParType SELECT TypeEmpl, count(*) AS nbEmpl FROM EMPLACEMENT GROUP BY TypeEmpl;
   #+END_SRC
** 3
   #+BEGIN_SRC sql
     CREATE TRIGGER UpdateNbPlaceParType
     AFTER INSERT ON NbPlacesParType
     FOR EACH ROW 
     EXECUTE PROCEDURE UpdateNbPlaceParType();

     CREATE FUNCTION UpdateNbPlaceParType()
     RETURNS TRIGGER
     As $$
     DECLARE
     BEGIN
     UPDATE NbPlacesParType SET TypeEmpl = new.TypeEmpl, NbEmpl = new.NbEmpl;
     END
     $$ LANGUAGE PLPGSQL;
   #+END_SRC
** 4
* Ex 2
* Ex 3
