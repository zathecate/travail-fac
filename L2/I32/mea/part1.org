* Ex 1
client(_numClient_, nomClient, adresseClient, #numTerritoire)
territoire(_numTerritoire_, nomTerritoire)
vendeur(_numVendeur_, nomVendeur, adresseVendeur)
modele(_numModele_, anneeModel, prixBase, nomModele)
option(_codeOption_, tarifOption)
commande(_numCommande_, dateCommande, acompte, #numClient, #numModele, #numVendeur)
comporte(_#numCommande, #codeOption_)
couvertPar(_#numTerritoire, #numVendeur_, anciennete)

* Ex 2
|--------------------------------+---------------------------+---------------------+----------------------------|
| *classe*                       |                           |                     | *Categorie*                |
| ~codeClasse~                   |                           |                     | ~codecategorie~            |
| libelleClasse                  | 0, n  <est de classe 1, 1 | <-----------------> | libelleCategorie           |
|                                |                           |                     | tarifMin                   |
|                                |                           |                     | tarifMax                   |
|--------------------------------+---------------------------+---------------------+----------------------------|
|                                |                           |                     | 0, n                       |
| 0, n <est de classe> 1, 1      |                           |                     | <Est de type>              |
|                                |                           |                     | 1, 1                       |
|--------------------------------+---------------------------+---------------------+----------------------------|
| *Logement*                     |                           |                     | *Chambre*                  |
| ~codeLogement~                 | 1, n <contient> 1, 1      | <---------------->  | ~codeChambre~              |
| adresseLogement                |                           |                     | nbreLits                   |
|                                |                           |                     | tarif                      |
|--------------------------------+---------------------------+---------------------+----------------------------|
| ^                              |                           |                     | 1, n <situe dans> 1, 1     |
|--------------------------------+---------------------------+---------------------+----------------------------|
| ^                              | *Client*                  |                     | *Sejour*                   |
|                                | ~codeClient~              |                     | ~codeSejour~               |
|                                | nomClient                 | 0, n <Reside> 0, 1  | dateDebut                  |
|                                | adresseClient             |                     | dateFin                    |
| v                              |                           |                     | nbPers                     |
|--------------------------------+---------------------------+---------------------+----------------------------|
| 1, n <tarificationLoisin> 1, n |                           | *Loisir*            |                            |
| tarifLoisir                    | <-----------------------> | ~codeLoisir~        | 0, n  <loisirSejour>  0, n |
|                                |                           | libelleLoisir       |                            |
|--------------------------------+---------------------------+---------------------+----------------------------|
