#include <stdlib.h>
#include <stdio.h>

typedef int* tableau;

int main(void) {

  int n = 0;
  scanf("%d", &n);
  tableau t = malloc(n * sizeof(int));
  for (int i = 0; i < n; i++) {
    t[i] = 0;
    printf("%d\n", t[i]);
  }

  free(t);


  return 0;
}
