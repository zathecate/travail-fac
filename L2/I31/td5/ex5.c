#include <stdio.h>

void printTab(int* tab, unsigned int n) {
  if (n > 0) {
    printf("%d\n", tab[n - 1]);
    printTab(tab, n - 1);
  }
}

int main(void) {

  int tab[5] = {12, 54, 87, 56, 23};
  printTab(tab, 5);

  return 0;
}
