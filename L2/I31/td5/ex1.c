#include <stdio.h>

void printNumber(int n) {
  printf("%d\n", n);
  if (n > 1)
    printNumber(n - 1);
}

int main(void) {

  printNumber(12);

  return 0;
}
