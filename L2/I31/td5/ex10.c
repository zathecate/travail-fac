#include <stdio.h>
#include <math.h>

typedef struct  { float x, y; } Point;

void printPoint(Point p) {
  printf("(%f, %f)\n", p.x, p.y);
}
float dist(Point a, Point b) {
  float d1 = a.x - b.x;
  float d2 = a.y - b.y;
  return sqrt(d1 * d1 + d2 * d2);
}

int main(void) {

  Point p = { 0, 0 };
  Point p2 = { 2, 1 };
  printf("%f\n", dist(p, p2));

  return 0;
}
