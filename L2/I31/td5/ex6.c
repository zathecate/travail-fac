#include <stdio.h>

int aux(int* tab, unsigned int n, unsigned int i, int acc) {
  if (i >= n) return acc;
  if (tab[i] > acc) return aux(tab, n, i + 1, tab[i]);
  else return aux(tab, n, i + 1, acc);
}
int max(int* tab, unsigned int n) {
  aux(tab, n, 0, tab[0]);
}

int main(void) {

  int tab[5] = {12, 01, 32, 14, 02};
  int m = max(tab, 5);
  printf("%d\n", m);

  return 0;
}
