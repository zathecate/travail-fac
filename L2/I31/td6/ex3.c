#include <stdio.h>
#include <stdlib.h>

typedef struct element {
	int val;
	struct element* next;

} element;
typedef element* liste;

// Ex 606
liste create(int x) {
	liste l = malloc(sizeof(liste));
	l->next = NULL;
	l->val = x;
	return l;
}
void delete(liste l) {
	liste it = l;
	while(it != NULL) {
		liste s = it->next;
		free(it);
		it = s;
	}
}

//Ex 603
void print(liste l) {
	printf("[");
	element* it = l;
	while (it != NULL) {
		printf("%d", it->val);
		if (it->next != NULL) printf(",");
		it = it->next;
	}
	printf("]\n");
	
}

// Ex 604
unsigned int count_r_aux(liste l, int acc) {
	if (l == NULL) 
		return acc;
	else
		return count_r_aux(l->next, acc + 1);
}
unsigned int count_r(liste l) {
	return count_r_aux(l, 0);
}
// Ex 605
unsigned int count_i(liste l) {
	int n = 0;
	liste it = l;
	while(it != NULL) {
		n++;
		it = it->next;
	}
	return n;
}

// Ex 607
liste append(liste l, element* pe) {
	liste it = l;
	while (it->next != NULL) it = it->next;
	it->next = pe;
	if (it->next == NULL) return NULL;
	else return l;
}

// Ex 608
liste appendv(liste l, int val) {
	liste it = l;
	liste pe = create(val);
	while (it->next != NULL) it = it->next;
	it->next = pe;
	if (it->next == NULL) return NULL;
	else return l;
}

// Ex 609
liste head(liste l, int val) {
	liste pe = create(val);
	pe->next = l;
	l = pe;
	if (pe == NULL) return NULL;
	else return pe;
}

// Ex 610
element* get(liste l, unsigned int index) {
	liste it = l;
	for (int i = 0; i < index; i++) {
		if(it == NULL) return NULL;
		else it = it->next;
	}
	return it;
}

// Ex 611
liste insert(liste l, unsigned int index, element* pe) {
	liste it = l;
	if(count_i(l) <= index) return NULL;
	if (index == 0) {
		pe->next = l;
		return pe;
	}
	for (int i = 0; i < index; count_i(l)) it = it->next;
	pe->next = it->next;
	it->next = pe;
	return l;
}

// Ex 612
liste insertv(liste l, unsigned int index, int val) {
	if (index >= count_i(l)) return NULL;
	liste e = create(val);
	l = insert(l, index, e);
	return l;
}

// Ex 613

liste suppress(liste l, element* pe) {
	if (l == NULL || pe == NULL) return NULL;
	if(l == pe) {
		liste n = l->next;
		free(l);
		return n;
	}
	liste it = l;
	while (it->next != pe) it = it->next;
	liste n = it->next->next;
	free(it->next);
	it->next = n;
	return l;		
}


// Ex 614
int find(liste l, int val) {
	liste it = l;
	int count = 0;
	while(it != NULL) {
		if(it->val == val) return count;
		it = it->next;
		count++;
	}
	return -1;
}

int main(void) {
	liste l = create(12);
	l->next = create(5);
	liste l2 = create(56);
	append(l, l2);
	l = suppress(l, l->next);
	print(l);


	delete(l);
	return 0;
}
