#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct Element {
	double value;
	struct Element* next;
} Element;

typedef struct {
	unsigned int size;
	Element* first;
	Element* last;
} Queue;

void print(Queue* q) {
	if (q != NULL) {
		printf("( ");
		Element* it = q->first;
		while (it != NULL) {
			printf("%lf", it->value);
			if (it->next != NULL) printf(", ");
			it = it->next;
		}
		printf(" )\n");
	}
}

Queue* newQueue() {
	Queue* q = malloc(sizeof(Queue));
	q->first = NULL;
	q->last = NULL;
	q->size = 0;
	return q;
}

Element* newElement(double val) {
	Element* e = malloc(sizeof(Element));
	e->next = 0;
	e->value = val;
	return e;
}

void destroy(Queue* q) {
	if (q != NULL) {
		Element* it = q->first;
		while (it != NULL) {
			Element* n = it->next;
			free(it);
			it = n;
		}
		free(q);
	}
}

int push(Queue* q, Element* e) {
	if (q == NULL) return 0;
	if (q->first == NULL) {
		q->first = e;
		q->last = e;
		q->size++;
		return 1;
	}
	Element* it = q->first;
	while (it->next != NULL) it = it->next;
	it->next = e;
	q->last = e;
	q->size++;
	return 1;
}

int pushv(Queue* q, double v) {
	if (q == NULL) return 0;
	Element* e = newElement(v);
	push(q, e);
	return 1;
}

Element* pop(Queue* q) {
	if (q == NULL || q->first == NULL) return NULL;
	Element* e = q->first;
	q->first = q->first->next;
	q->size--;
	return e;
}

double popv(Queue* q) {
	Element* e = pop(q);
	if (e == NULL) return NAN;
	double v = e->value;
	free(e);
	return v;
}

int main(void) {
	Queue* q = newQueue();
	pushv(q, 21.594);
	pushv(q, 54.1);
	pushv(q, 300.10);
	print(q);
	double val = popv(q);
	print(q);
	printf("%lf\n", val);

	destroy(q);
	return 0;
}
