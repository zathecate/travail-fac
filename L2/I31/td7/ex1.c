#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int allocs = 0;

typedef struct Element {
	float value;
	struct Element* next;
} Element;
typedef struct {
	unsigned int size;
	Element* first;
} Stack;

void destroy(Stack* p) {
	Element* it = p->first;
	while (it != NULL) {
		Element* n = it->next;
		free(it);
		allocs--;
		it = n;
	}
	free(p);
	allocs--;
}

Stack* newStack() {
	Stack* s = malloc(sizeof(Stack));
	allocs++;
	s->size = 0;
	return s;
}

Element* newElement(float val) {
	Element* e = malloc(sizeof(Element));
	allocs++;
	e->next = NULL;
	e->value = val;
	return e;
}

void print(Stack* p) {
	Element* it = p->first;
	printf("( ");
	while (it != NULL) {
		printf("%f", it->value);
		if (it->next != NULL)
			printf(", ");
		it = it->next;
	}
	printf(" )\n");
}

int push(Stack* p, Element* e) {
	if (p == NULL) return 0;
	if (p->first == NULL) {
		p->first = e;
		p->size++;
		return 1;
	}
	Element* it = p->first;
	while (it->next != NULL) it = it->next;
	it->next = e;
	p->size++;
	return 1;
}

int pushv(Stack* p, float v) {
	if (p == NULL) return 0;
	Element* e = newElement(v);
	int res = push(p, e);
	return res;
}

Element* pop(Stack* p) {
	if (p == NULL || p->first == NULL) return NULL;
	if (p->first->next == NULL) {
		Element* e = p->first;
		p->first = NULL;
		p->size--;
		e->next = NULL;
		return e;
	}
	Element* it = p->first;
	while (it->next->next != NULL) it = it->next;
	Element* e = it->next;
	it->next = NULL;
	p->size--;
	e->next = NULL;
	return e;
}

float popv(Stack* p) {
	if (p == NULL) return NAN;
	Element* e = pop(p);
	if (e == NULL) return NAN;
	float v = e->value;
	free(e);
	allocs--;
	return v;
}

int main(void) {
	Stack* s = newStack();
	pushv(s, 78.1);
	pushv(s, 36.0);
	pushv(s, 48.0);
	print(s);
	printf("Allocs: %d\n", allocs);
	destroy(s);
	printf("Allocs: %d\n", allocs);
	return 0;
}
