#include <stdio.h>
#include <stdlib.h>
#include <limits.h>


typedef struct Node {
	int val;
	struct Node* right;
	struct Node* left;
} Node;

typedef struct {
	Node* root;
} Tree;

Node* newNode(int v) {
	Node* n = malloc(sizeof(Node));
	n->val = v;
	n->right = NULL;
	n->left = NULL;
	return n;
}

Tree* newTree(Node* r) {
	Tree* t = malloc(sizeof(Tree));
	t->root = r;
	return t;
}

void destroyNode(Node* n) {
	if (n != NULL) {
		Node* l = n->left;
		Node* r = n->right;
		free(n);
		destroyNode(l);
		destroyNode(r);
	}
}

int setLeft(Node* n, Node* l) {
	if (n == NULL) return 0;
	if (n->left != NULL)
		destroyNode(n->left);
	n->left = l;
	return 1;
}
int setRight(Node* n, Node* r) {
	if (n == NULL) return 0;
	if (n->right != NULL)
		destroyNode(n->right);
	n->right = r;
	return 1;
}

int countNodes(Node* r) {
	if (r == NULL) return 0;	
	int res = 1;
	res += countNodes(r->left);
	res += countNodes(r->right);
	return res;
}

void printNodes(Node* r) {
	if (r != NULL) {
		printNodes(r->left);
		if (r->left != NULL) printf(", ");
		printf("%d", r->val);
		if (r->right != NULL) printf(", ");
		printNodes(r->right);
	}
}

Node* search(Node* r, int val) {
	if (r != NULL) {
		if (r->val == val) return r;
		Node* res1 = search(r->left, val);
		Node* res2 = search(r->right, val);
		if (res1 != NULL) return res1;
		return res2;
	}
}

int min(Node* r) {
	if (r != NULL) {
		int res = r->val;
		if (r->left != NULL) {
			int resL = min(r->left);
			if (resL < res) res = resL;
		}
		if (r->right != NULL) {
			int resR = min(r->right);
			if (resR < res) res = resR;
		}
		return res;
	}
	return INT_MIN;
}
int max(Node* r) {
	if (r != NULL) {
		int res = r->val;
		if (r->left != NULL) {
			int resL = max(r->left);
			if (resL > res) res = resL;
		}
		if (r->right != NULL) {
			int resR = max(r->right);
			if (resR > res) res = resR;
		}
		return res;
	}
	return INT_MAX;
}

int depth(Node* r) {
	if (r == NULL) return 0;
	int res1 = 1 + depth(r->left);
	int res2 = 1 + depth(r->right);
	return res1 > res2 ? res1 : res2;
}

int main(void) {

	Node* root = newNode(25);
	Node* r = newNode(45);
	Node* l = newNode(17);
	Node* ll = newNode(73);
	setLeft(l, ll);
	setLeft(root, l);
	setRight(root, r);
	printf("Nodes: %d\n", countNodes(root));
	printNodes(root);
	printf("Depth: %d\n", depth(root));
	destroyNode(root);


	return 0;
}
