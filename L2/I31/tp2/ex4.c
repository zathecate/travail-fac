#include <stdio.h>

int main(void) {

	int n;
	printf("Saisissez un nombre: ");
	scanf("%d", &n);
	if (n <= 0) {
		perror("Erreur: Le nombre saisi est inférieur ou égal à 0\n");
		return -1;
	}
	for (int i = 1; i <= n; i++)
		printf("%d\n", i*i);

	return 0;
}
