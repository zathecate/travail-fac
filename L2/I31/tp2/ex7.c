#include <stdio.h>

int fac(int n) {
	int res = 1;
	while (n > 0)
		res *= n--;
	return res;
}

float power(float x, int n) {
	int res = 1;
	while (n > 0) {
		res *= x;
		n--;
	}
	return res;
}

int binom(int k, int n) {
	return fac(n) / (fac(k) * fac(n - k));
}

float f(float x, float y, int n) {
	int res = 0;
	for (int i = 0; i <= n; i++) {
		res += (binom(i, n) * power(x, n - i) * power(y, i));
	}
	return res;
}

int main() {

	float x, y;
	int n;
	scanf("%f %f %d", &x, &y, &n);
	printf("%f\n", f(x, y, n));


	return 0;
}

