#include <stdio.h>

int main(void) {

	int n, chiffres = 1;
	printf("Saisissez un nombre: ");
	scanf("%d", &n);
	do {
		n = (int) n / 10;
		chiffres++;
	} while ((int) n / 10 != 0);

	printf("Il y a %d chiffres dans le nombre\n", chiffres);

	return 0;
}
