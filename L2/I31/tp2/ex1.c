#include <stdio.h>

void f() {

	int a, b;
	char op;
	scanf("%d %c %d", &a, &op, &b);
	switch (op) {
		case ('+'):
			printf("%d\n", a + b);
			break;
		case ('-'):
			printf("%d\n", a - b);
			break;
		case ('*'):
			printf("%d\n", a * b);
			break;
		case ('/'):
			printf("%d\n", a / b);
			break;
		case ('%'):
			printf("%d\n", a % b);
			break;
	}
}

int main() {
	int continuer = 1;
	char c;
	while (continuer) {
		f();
		printf("Continuer [O, n]: ");
		scanf(" %c", &c);
		if (c == 'o' || c == 'O' || c == '\0')
			continuer = 1;
		else
			continuer = 0;
	}
}
