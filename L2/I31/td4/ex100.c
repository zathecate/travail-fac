#include <stdio.h>

int recherche(int x, int t[], unsigned int n) {
  for (int i = 0; i < n; i++)
    if (t[i] == x) return i;
  return -1;
}

int main(void) {

  int t[5] = {56, 48, 12, 32, 1};
  int i = recherche(120, t, 5);
  printf("%d\n", i);

  return 0;
}
