#include <stdio.h>

void affiche(int t[], unsigned int n) {
  if (n == 0) printf("[]");
  else {
    printf("[");
    for (int i = 0; i < n - 1; i++)
      printf("%d, ", t[i]);
    printf("%d]", t[n - 1]);
  }
}

int main(void) {

  int t[5] = {2, 65, 98, 14, 2};
  affiche(t, 5);


  return 0;
}
