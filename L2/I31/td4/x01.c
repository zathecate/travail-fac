#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int int_min(int t[], unsigned int n) {
  if (n == 0) return INT_MAX;
  int minimum = t[0];
  for (int i = 0; i < n; i++)
    if (t[i] < minimum)
      minimum = t[i];
  return minimum;
}
int main(void) {

  int tab[3] = {-5, 56, 2};
  int m = int_min(tab, 3);
  printf("%d\n", m);

  return 0;
}
