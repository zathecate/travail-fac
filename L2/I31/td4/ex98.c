#include <stdio.h>

int main(void) {

  int tab[5];
  scanf(" %d", &tab[0]);
  for (int i = 1; i < 5; i++)
    scanf(" %d", &tab[i]);
  for (int i = 4; i >= 0; i--)
    printf("%d ", tab[i]);
  printf("\n");

  return 0;
}
