#include <stdio.h>
#include <stdlib.h>

void echange(int* a, int* b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

int main(void) {

  int a = 5, b = 3;
  echange(&a, &b);
  printf("a = %d, b = %d\n", a, b);


  return 0;
}
