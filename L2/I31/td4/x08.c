#include <stdio.h>
#include <stdlib.h>
void affiche(int t[], unsigned int n) {
  if (n == 0) printf("[]");
  else {
    printf("[");
    for (int i = 0; i < n - 1; i++)
      printf("%d, ", t[i]);
    printf("%d]", t[n - 1]);
  }
}

int main(void) {

  unsigned int rows;
  unsigned int cols;
  printf("Nombre de lignes: ");
  scanf("%d", &rows);
  printf("Nombre de colonnes: ");
  scanf(" %d", &cols);
  int t[rows][cols];
  for (int i = 0; i < rows; i++)
    for (int j = 0; j < cols; j++) {
      printf("Entrez une valeur pour t[%d][%d]: ", i, j);
      scanf(" %d", &t[i][j]);
    }
  printf("[\n");
  for (int i; i < rows; i++) {
    affiche(t[i], cols);
    printf("\n");
  }
  printf("]");

  return 0;
}
